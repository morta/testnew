import { Component, OnInit, EventEmitter, Output} from '@angular/core';
import {Invoice} from './invoice';


@Component({
  selector: 'jce-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],
  inputs:['invoice']
})
export class InvoiceComponent implements OnInit {

  invoice:Invoice;
  isEdit:Boolean = false;

  @Output() invouceAdded = new EventEmitter<Invoice>();


  constructor() { }

  ngOnInit() {
  }

}
