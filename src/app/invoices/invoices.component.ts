import { Component, OnInit } from '@angular/core';
import { InvoicesService } from './invoices.service';

@Component({
  selector: 'jce-invoices',
  templateUrl: './invoices.component.html',
  styles: [`
    .invoices li { cursor: default; }
    .invoices li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }  
  `]
})
export class InvoicesComponent implements OnInit {
  isLoading:Boolean = true;
  invoices;
  currentInvoice;

  select(invoice){
  this.currentInvoice = invoice; 
  }

  addInvoice(invoice){
    //this.invoices.push(invoice)
    this._invoicesService.addInvoice(invoice);
    }

  constructor(private _invoicesService:InvoicesService) { }

  ngOnInit() {
    this._invoicesService.getInvoices().subscribe(invoicesData => 
    {this.invoices = invoicesData;
    this.isLoading = false;
    console.log(this.invoices)});
  }

}
