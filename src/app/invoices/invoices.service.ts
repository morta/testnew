import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';
import {Http} from '@angular/http';
import 'rxjs/add/operator/delay';

@Injectable()
export class InvoicesService {

invoicesObservable;

 getInvoices() {
      //return this._http.get(this._url).map(res =>res.json()).delay(2000);
      this.invoicesObservable = this.af.database.list('/invoices');
      return this.invoicesObservable.delay(2000);
     }

  addInvoice(invoice){
      this.invoicesObservable.push(invoice);
    }

  constructor(private af:AngularFire) { }

}
