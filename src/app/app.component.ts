import { Component } from '@angular/core';
import {AngularFire} from 'angularfire2'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  //title = 'angular 2.0 is installed very successfully';
  title = 'Invoices!';
  constructor (af:AngularFire) {
    console.log(af);
  }
}
